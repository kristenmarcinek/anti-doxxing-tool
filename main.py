import re
import os
import requests
import pickle
import hashlib

from doxxer import create_filename

from bs4 import BeautifulSoup

<<<<<<< HEAD
print('Welcome! This is an anti-doxxing tool')
print('The purpose of this tool is to notify you of whether your information has been leaked.')
print('Do not be alarmed if there is a large amount of search results, this may just be the search query being processed.')
print('The program will tell you whether it thinks your information has actually been leaked.')
print('To begin, input your information like your first-middle-last name, your phone number, address, email, or commonly used passwords.')
print('Please be specific. If you simply put John Smith, this is not a good indicator of whether you have been doxxed. Please be wary of inputting sensitive information such as your SSN or license number. While the information is secure on our end, it will be ran through a search of several websites, including Google.')

user_input = input("What would you like to find?: ")

if user_input.isdigit():
    userInput = user_input
else:
    userInput = user_input.lower()

<<<<<<< HEAD
userInputs = [userInput1, userInput2, userInput3]
pages = [0, 10, 20, 30, 40]
gitpages = [1, 2, 3, 4, 5]

for userInput, page in zip(userInputs, pages):
    reddit = "https://www.google.com/search?q=site:reddit.com+{0}&client=firefox-b-1-d&ei=HSiiXrDVB-bAytMPjY2EgAY&start={1}&sa=N&ved=2ahUKEwjw19mE3f_oAhVmoHIEHY0GAWAQ8tMDegQICxAs&biw=1200&bih=674".format(userInput, page)
    fourchan = "https://find.4chan.org/?q={0}&o={1}".format(userInput, page)
    ninegag = "https://www.google.com/search?q=site:9gag.com+{0}&client=firefox-b-1-d&ei=kEWiXtXRC4OoytMP1t2xEA&start={1}&sa=N&ved=2ahUKEwjV2c2P-f_oAhUDlHIEHdZuDAIQ8tMDegQIChAs&biw=1200&bih=674".format(userInput, page)
    pastebin = "https://www.google.com/search?q=site:pastebin.com+{0}&client=firefox-b-1-d&ei=wEWiXrSvN7ewytMPuv2b0AY&start={1}&sa=N&ved=2ahUKEwi0j-um-f_oAhU3mHIEHbr-BmoQ8tMDegQICxAs&biw=1200&bih=674".format(userInput, page)
    twitter = "https://twitter.com/search?q={0}&src=typed_query".format(userInput)
    facebook = "https://www.google.com/search?q=site:facebook.com+{0}&client=firefox-b-1-d&ei=D0aiXpecGMqvytMPyse7gAk&start={1}&sa=N&ved=2ahUKEwjX36HM-f_oAhXKl3IEHcrjDpAQ8tMDegQICxAx&biw=1200&bih=674".format(userInput, page)
    tumblr = "https://www.tumblr.com/search/{0}".format(userInput)

for userInput, gitpage in zip(userInputs, gitpages):
    github = "https://github.com/search?p={0}&q={1}&type=Repositories".format(gitpage, userInput)
=======
reddit = "https://www.google.com/search?q=site:reddit.com+{0}&client=firefox-b-1-d&ei=HSiiXrDVB-bAytMPjY2EgAY&start=0&sa=N&ved=2ahUKEwjw19mE3f_oAhVmoHIEHY0GAWAQ8tMDegQICxAs&biw=1200&bih=674".format(userInput)
fourchan = "https://find.4chan.org/?q={0}&o=0".format(userInput)
ninegag = "https://www.google.com/search?q=site:9gag.com+{0}&client=firefox-b-1-d&ei=kEWiXtXRC4OoytMP1t2xEA&start=0&sa=N&ved=2ahUKEwjV2c2P-f_oAhUDlHIEHdZuDAIQ8tMDegQIChAs&biw=1200&bih=674".format(userInput)
pastebin = "https://www.google.com/search?q=site:pastebin.com+{0}&client=firefox-b-1-d&ei=wEWiXrSvN7ewytMPuv2b0AY&start=0&sa=N&ved=2ahUKEwi0j-um-f_oAhU3mHIEHbr-BmoQ8tMDegQICxAs&biw=1200&bih=674".format(userInput)
twitter = "https://twitter.com/search?q={0}&src=typed_query".format(userInput)
facebook = "https://www.google.com/search?q=site:facebook.com+{0}&client=firefox-b-1-d&ei=D0aiXpecGMqvytMPyse7gAk&start=0&sa=N&ved=2ahUKEwjX36HM-f_oAhXKl3IEHcrjDpAQ8tMDegQICxAx&biw=1200&bih=674".format(userInput)
tumblr = "https://www.tumblr.com/search/{0}".format(userInput)
github = "https://github.com/search?p=1&q={0}&type=Repositories".format(userInput)
>>>>>>> commits

reddit_data1 = requests.get(reddit)
fourchan_data1 = requests.get(fourchan)
ninegag_data1 = requests.get(ninegag)
pastebin_data1 = requests.get(pastebin)
twitter_data1 = requests.get(twitter)
facebook_data1 = requests.get(facebook)
github_data1 = requests.get(github)
tumblr_data1 = requests.get(tumblr)

soup = BeautifulSoup(reddit_data1.text, "html.parser")
soup2 = BeautifulSoup(fourchan_data1.text, "html.parser")
soup3 = BeautifulSoup(ninegag_data1.text, "html.parser")
soup4 = BeautifulSoup(pastebin_data1.text, "html.parser")
soup5 = BeautifulSoup(twitter_data1.text, "html.parser")
soup6 = BeautifulSoup(facebook_data1.text, "html.parser")
soup7 = BeautifulSoup(github_data1.text, "html.parser")
soup8 = BeautifulSoup(tumblr_data1.text, "html.parser")

<<<<<<< HEAD
for userInput in userInputs:
    text1 = soup.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text2 = soup2.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text3 = soup3.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text4 = soup4.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text5 = soup5.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text6 = soup6.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text7 = soup7.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
    text8 = soup8.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
=======
text1 = soup.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text2 = soup2.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text3 = soup3.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text4 = soup4.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text5 = soup5.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text6 = soup6.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text7 = soup7.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
text8 = soup8.find_all(string=re.compile('.*{0}.*'.format(userInput)), recursive=True)
>>>>>>> commits

user_output = len(text1) + len(text2) + len(text3) + len(text4) + len(text5) + len(text6) + len(text7) + len(text8)

class finalOutput:
    print ('Found the word "{0}" {1} times\n'.format(user_input, user_output))
    if user_output > 75:
        print ('Your information has been breached')
    else:
        print('Your information appears secure')
=======
>>>>>>> 341f919fb7f17ceecfee27d13fca3968348eb093

def query_and_cache_data(user_input):
    if user_input.isdigit():
        userInput1 = user_input
        userInput2 = user_input
        userInput3 = user_input
    else:
        userInput1 = user_input.lower()
        userInput2 = user_input.title()
        userInput3 = user_input.upper()

    userInputs = [userInput1, userInput2, userInput3]

    pages = [0, 10, 20, 30, 40]
    gitpages = [1, 2, 3, 4, 5]

    for userInput, page in zip(userInputs, pages):
        reddit = "https://www.google.com/search?q=site:reddit.com+{0}&client=firefox-b-1-d&ei=HSiiXrDVB-bAytMPjY2EgAY&start={1}&sa=N&ved=2ahUKEwjw19mE3f_oAhVmoHIEHY0GAWAQ8tMDegQICxAs&biw=1200&bih=674".format(
            userInput, page)
        fourchan = "https://find.4chan.org/?q={0}&o={1}".format(userInput, page)
        ninegag = "https://www.google.com/search?q=site:9gag.com+{0}&client=firefox-b-1-d&ei=kEWiXtXRC4OoytMP1t2xEA&start={1}&sa=N&ved=2ahUKEwjV2c2P-f_oAhUDlHIEHdZuDAIQ8tMDegQIChAs&biw=1200&bih=674".format(
            userInput, page)
        pastebin = "https://www.google.com/search?q=site:pastebin.com+{0}&client=firefox-b-1-d&ei=wEWiXrSvN7ewytMPuv2b0AY&start={1}&sa=N&ved=2ahUKEwi0j-um-f_oAhU3mHIEHbr-BmoQ8tMDegQICxAs&biw=1200&bih=674".format(
            userInput, page)
        twitter = "https://twitter.com/search?q={0}&src=typed_query".format(userInput)
        facebook = "https://www.google.com/search?q=site:facebook.com+{0}&client=firefox-b-1-d&ei=D0aiXpecGMqvytMPyse7gAk&start={1}&sa=N&ved=2ahUKEwjX36HM-f_oAhXKl3IEHcrjDpAQ8tMDegQICxAx&biw=1200&bih=674".format(
            userInput, page)
        tumblr = "https://www.tumblr.com/search/{0}".format(userInput)

    for userInput, gitpage in zip(userInputs, gitpages):
        github = "https://github.com/search?p=(0)&q=(1)&type=Repositories".format(gitpage, userInput)

    reddit_data1 = requests.get(reddit)
    fourchan_data1 = requests.get(fourchan)
    ninegag_data1 = requests.get(ninegag)
    pastebin_data1 = requests.get(pastebin)
    twitter_data1 = requests.get(twitter)
    facebook_data1 = requests.get(facebook)
    github_data1 = requests.get(github)
    tumblr_data1 = requests.get(tumblr)

    data = {
        'reddit': reddit_data1,
        'fourchan': fourchan_data1,
        'ninegag': ninegag_data1,
        'pastebin': pastebin_data1,
        'twitter': twitter_data1,
        'facebook': facebook_data1,
        'github': github_data1,
        'tumblr': tumblr_data1
    }

    filename = create_filename(user_input)
    pickle.dump(data, open(filename, 'wb'))
    return data


def load_cached_data(user_input):
    filename = create_filename(user_input)
    data = pickle.load(open(filename, 'rb'))
    return data


def main():
    print('Welcome! This is an anti-doxxing tool')
    print('The purpose of this tool is to notify you of whether your information has been leaked.')
    print('Do not be alarmed if there is a large amount of search results, this may just be the search query being processed.')
    print('The program will tell you whether it thinks your information has actually been leaked.')
    print('To begin, input your information like your first-middle-last name, your phone number, address, email, or commonly used passwords.')
    print('Please be specific. If you simply put John Smith, this is not a good indicator of whether you have been doxxed. Please be wary of inputting sensitive information such as your SSN or license number. While the information is secure on our end, it will be ran through a search of several websites, including Google.')

    user_input = input("What would you like to find?: ")

    filename = create_filename(user_input)

    # Cache exists...
    if os.path.exists(filename):
        data = load_cached_data(user_input)
    else:
        data = query_and_cache_data(user_input)

    reddit_data1 = data['reddit']
    fourchan_data1 = data['fourchan']
    ninegag_data1 = data['ninegag']
    pastebin_data1 = data['pastebin']
    twitter_data1 = data['twitter']
    facebook_data1 = data['facebook']
    github_data1 = data['github']
    tumblr_data1 = data['tumblr']


    soup = BeautifulSoup(reddit_data1.text, "html.parser")
    soup2 = BeautifulSoup(fourchan_data1.text, "html.parser")
    soup3 = BeautifulSoup(ninegag_data1.text, "html.parser")
    soup4 = BeautifulSoup(pastebin_data1.text, "html.parser")
    soup5 = BeautifulSoup(twitter_data1.text, "html.parser")
    soup6 = BeautifulSoup(facebook_data1.text, "html.parser")
    soup7 = BeautifulSoup(github_data1.text, "html.parser")
    soup8 = BeautifulSoup(tumblr_data1.text, "html.parser")

    # TODO: handle multiple user inputs
    # for userInput in userInputs:
    #     pass

    text1 = soup.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text2 = soup2.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text3 = soup3.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text4 = soup4.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text5 = soup5.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text6 = soup6.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text7 = soup7.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)
    text8 = soup8.find_all(string=re.compile('.*{0}.*'.format(user_input)), recursive=True)

    user_output = len(text1) + len(text2) + len(text3) + len(text4) + len(text5) + len(text6) + len(text7) + len(text8)

    print ('Found the word "{0}" {1} times\n'.format(user_input, user_output))
    if user_output > 75:
        print('Your information has been breached')
    else:
        print('Your information appears secure')


if __name__ == '__main__':
    main()
