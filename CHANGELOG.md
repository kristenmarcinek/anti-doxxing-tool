# Changelog

## 2020-04-23
### Added
- main.py, the scraper itself
- web-scraper-test.ipynb, the notebook

## 2020-04-21
### Added
- changelog

### Updated
- README

### Removed
- Previous files