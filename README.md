# Anti-Doxxing Web Scraper

Web scraper utilizing Python 3.8 and BeautifulSoup to notify the user of if their private information has been leaked.

Kristen Marcinek's Out in Tech project.
