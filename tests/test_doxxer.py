import os
from doxxer import create_filename


class TestCreateFilename:
    expected_result = os.path.join('cache', '5fb40e89fb49ac58f5a951f8eac0984768664ab0b3dfae2bd49863ae_cache.p')

    def test_camelcase_with_whitespace(self):
        result = create_filename('KrisTen ')
        assert result == self.expected_result

    def test_camelcase_with_no_whitespace(self):
        result = create_filename('Kristen')
        assert result == self.expected_result

    def test_uppercase(self):
        result = create_filename('KRISTEN')
        assert result == self.expected_result

    def test_lowercase(self):
        result = create_filename('kristen')
        assert result == self.expected_result
