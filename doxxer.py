import os
import hashlib


def create_filename(name):
    hash = hashlib.sha224(name.strip().lower().encode('utf-8')).hexdigest()
    filename = '{}_cache.p'.format(hash)
    path_to_cache = os.path.join('cache', filename)
    return path_to_cache
